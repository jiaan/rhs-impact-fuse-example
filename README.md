# RHS Impact Fuse Example

This script shows how to use an ammunition effect to call a script to simulate impact fuses, in this case for the [RGN/O](https://en.wikipedia.org/wiki/RGN_hand_grenade) grenades as seen in [RHS mod](http://www.rhsmods.org) for ARMA 3. 