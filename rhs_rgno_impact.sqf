params [
	"_shooter",
	"_grenade",
	["_forceExec", false]
];

// Exit on bad input
if (not( local _shooter)) exitWith {};

// Get RHS submunition, exit on empty
private _sub = getText(configFile >> "cfgAmmo" >> typeOf _grenade >> "rhs_submunition");
if (_sub == "") exitWith {};

// Get the parent's position and velocity
private _pos = getPos _grenade;
private _vel = velocity _grenade;

// Generate event ID
private _id = "RHS_pfh_rgno_" + str _grenade;

// Delete parent grenade
deleteVehicle _grenade;

// Create new submunition grenade with parent's position and velocity
_grenade = createVehicle [_sub, _pos, [], 0, "CAN_COLLIDE"];
_grenade setVectorUp (vectorNormalized _vel);
_grenade setVelocity _vel;
[_grenade,[vehicle _shooter,_shooter]]  remoteExecCall ["setShotParents",2];

// Get RHS impact explosion ammo, exit if empty
private _explosion = getText(configFile >> "cfgAmmo" >> typeOf _grenade >> "rhs_impact");
if (_explosion == "") exitWith {};

//systemChat str ["Start", time];

// On each frame
[_id, "onEachFrame", {
	params ["_grenade","_startTime","_lastTime","_wait","_impacted","","","_shooter"];

	// Wait for 1 second until fuze activates
	if (_wait) exitWith {
		if ((time - _startTime) >= 1) then {
			_this set [3, false];
			// Exit script if grenade is resting on top of a surface
			if (vectorMagnitude(velocity _grenade) < 5) then {
				private _posASL = getPosASL _grenade;
				private _intersections = lineIntersectsSurfaces [_posASL, _posASL vectorAdd [0,0,-0.25], _grenade, objNull, true, 1, "FIRE", "NONE"];
				if (count _intersections > 0 || (getPosATL _grenade) select 2 < 0.5) then {
					[_this select 6, "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
				};
			};
			//systemChat str ["Done waiting", time];
		};
	};

	// Run at a maximum of 30hz
	private _delta = time - _lastTime;
	if (_delta >= 0.033) then {

		// Exit and clear on null grenade
		if (isNull _grenade) exitWith {
			[_this select 6, "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
		};

		// On impact wait 0.1 second, then exit and clear with explosion
		if (_impacted) exitWith {
			if (_delta >= 0.1) then {
				[_this select 6, "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
				private _pos = getPos _grenade;
				deleteVehicle _grenade;
				private _nil = createVehicle [_this select 5, _pos, [], 0, "CAN_COLLIDE"];
				[_nil,[vehicle _shooter,_shooter]]  remoteExecCall ["setShotParents",2];
				//systemChat str ["Exploded", _delta, time];
			};
		};

		//systemChat str ["Loop", _delta, time];

		// Update time
		_this set [2, time];

		private _impact = false;
		private _posASL = getPosASL _grenade;

		// Adaptive scan radius
		private _scanRad = 0.4 * (0.033 / (_delta Min 0.033));

		// Get intersections along all 3 axis
		private _intersections = lineIntersectsSurfaces [_posASL vectorAdd [_scanRad,0,0], _posASL vectorAdd [-_scanRad,0,0], _grenade, objNull, true, 1, "FIRE", "NONE"]
				+ lineIntersectsSurfaces [_posASL vectorAdd [0,_scanRad,0], _posASL vectorAdd [0,-_scanRad,0], _grenade, objNull, true, 1, "FIRE", "NONE"]
				+ lineIntersectsSurfaces [_posASL vectorAdd [0,0,_scanRad], _posASL vectorAdd [0,0,-_scanRad], _grenade, objNull, true, 1, "FIRE", "NONE"];

		// Explode if in contact with an object other than a bush
		if (count _intersections > 0) then {
			if (getObjectType (_intersections select 0 select 3) == 1) then {
				if (str (_intersections select 0 select 3) find ": b_" < 0) then {
					_impact = true;
				};
			} else {
				_impact = true;
			};
		};

		// Explode if in contact with ground
		if (!_impact && {((getPosATL _grenade) select 2) < _scanRad / 8}) then {
			_impact = true;
		};

		// Do explosion on impact
		if (_impact) then {
			_this set [4, true];
			//systemChat str ["Impact", _delta, time];
		};
	};

}, [_grenade, time, time, true, false, _explosion, _id,_shooter]] call BIS_fnc_addStackedEventHandler;